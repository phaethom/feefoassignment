# README #


### Feefo Software Engineering Technical Assessment ###

###Algorithms

---
Problems 1 and 2 solutions it's located inside _algorithms_ folder on this root repository.

######Instructions
Command line runner

Navigate to src folder and run the following commands:

  - javac com/engitronics/Main.java
  - java com.engitronics.Main
---
###UI Assessment

This assessment can be founded on root repository inside _ui-assessment-master_ folder.

####Installation and Setup Instructions

Make sure `node` and `npm` are installed on your machine. More information at [nodejs](https://nodejs.org/en/download/).

Clone or download this repository

Update packages:

   `npm install`

start the Application:

   `npm start`

Run tests:

   `npm run test`
Show application:

Depending on your host and port

   `http://localhost:3000 `


---------------------------------------------


###Web App Restful API System Design



1. **Describe high level design**

![](./highLevel.jpg)

The system is composed by 4 main services:
    
    1. Resource server
        - Running in Resource owner( desktop/mobile/tablet), React application, consuming resources located in resource server.
    2. Resource server
        - Protected resource providing end-points that grant rest operations through http/https methods to Resource owner.
    3. Database
        - Persistence Service through MySQL database storing all data.
    4. Authorisation Server
        - Implements security to the system through token access. 

2. **Web App UI**

wireframe with all basics pages and components.
![](./wireframe.jpg)


3. **Data Model**

Entity relation diagram

![](./dbSchema.png)

Class diagram

![](./class.jpg)

4. **Restful API**
   
   Requests are based on JSON object;
   Example:
````
    {"title":"Call Feefo",
    "userId":"Jones",
    "note": Call Feefo at 12pm,
    "priorityType": "Normal"}
````

Endpoints and API responses

| Operation | URI | Method | Status Code |
|---|---|---|---|
|Retrieve a note|https://{host}:{port}/api/note/{note id}|GET|200 404 500 |  
|Retrieve notes list|https://{host}:{port}/api/notes|GET|200 404 500|
|Delete a note|https://{host}:{port}/api/note/{note id}|DELETE|200 204 404 500|
|Save a note|https://{host}:{port}/api/note|POST|201 400 500|

Error codes

|Code|Definition|
|---|---|
|200|Success|
|201|Created|
|204|No Content|
|400|Bad Request|
|404|Not Found|
|500|Internal Server Error|

5. **Web Server**

Tomcat server embedded in Spring boot framework will be the ideal web server providing a fast system development.

--------------------------------------------



   


