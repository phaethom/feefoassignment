package com.engitronics;

import com.engitronics.problem1.Normaliser;
import com.engitronics.problem2.Stats;

public class Main {

    public static void main(String[] args) {

        Normaliser normaliser = new Normaliser();

        String normalised = normaliser.normalise("Chief Accountant");
        System.out.println(normalised);

        Stats stats = new Stats();
        int[] bigArray = stats.generateSomeHugeArray();

        stats.arrayManipulator(bigArray);
    }
}
