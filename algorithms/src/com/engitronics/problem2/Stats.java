package com.engitronics.problem2;

import java.util.Arrays;


public class Stats {
    public int[] generateSomeHugeArray(){
        int max=1000;
        int min = 1;
        int[] numbers;
        int random_int = (int)(Math.random() * (max - min + 1) + min);
        numbers = new int[random_int];

        for (int i = 0; i <numbers.length; i++) {
            int randomValue = (int)(Math.random() * (max - min + 1) + min);
            numbers[i]=randomValue;
        }
        return numbers;
    }

    public double calculateMedian(int[]num){
        double median=0;
        Arrays.sort(num);
        if (num.length%2==0){
            median= ((double)num[num.length/2] + (double)num[num.length/2 -1])/2;
        }else median = (double) num[num.length/2];
        return median;
    }

    public float calculateMean(int[]num){
        float total=0;
        for (int j : num) {
            total += j;
        }
        return total/num.length;
    }

    public void arrayManipulator(int[]num){
        double median = calculateMedian(num);
        System.out.println("Median = "+median);
        float mean = calculateMean(num);
        System.out.println("Mean = "+mean);
        int range= calculateRange(num);
        System.out.println("Range = "+range);
        int mode = calculateMode(num);
        System.out.println("Mode = "+mode);
    }

    public int calculateMode(int[]num){

        int maxNumber=-1;
        int maxAppearances = -1;
        for (int k : num) {
            int count = 0;
            for (int i : num) {
                if (k == i) {
                    count++;
                }
                if (count > maxAppearances) {
                    maxNumber = k;
                    maxAppearances = count;
                }
            }
        }
        return maxNumber;
    }

    public  int calculateRange(int[]num){
        int min = num[0];
        int max = num[0];

        for (int i = 1; i < num.length; i++) {
            if (num[i]>max){
                max=num[i];
 //               System.out.println("MAX  "+max);
            }
            if (num[i]<min){
                min=num[i];
    //            System.out.println("MIN  "+min);
            }
        }
        return max-min;
    }
}