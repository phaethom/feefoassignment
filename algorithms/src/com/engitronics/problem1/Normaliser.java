package com.engitronics.problem1;

import java.util.ArrayList;
import java.util.List;

public class Normaliser {

    private List<String> listJobs;

    public Normaliser() {
    }

    public List<String> getListJobs() {
        return listJobs;
    }

    public void setListJobs(ArrayList<String> listJobs) {
        this.listJobs = listJobs;
    }

    public String normalise(String jobTitle){

        String ret=null;
        if(jobTitle.contains("Java")){
            ret=jobTitle.replace("Java","Software");
        }else if (jobTitle.contains("C#")){
            ret=jobTitle.replace("C#","Software");
        }else if (jobTitle.contains("Chief")){
            ret=jobTitle.replace("Chief","");
        }
        return ret;
    }
}
