import React from 'react';
import './account-overview.css';
import {TopContainer, BottomContainer, Container, TitleContainer, ContactDetailsContainer, SalesStatsContainer} from './style'
import {SupportDetails} from './components/supportDetails/SupportDetails';
import {Title} from './components/common/title/Title'
import {Sales} from './components/sales/Sales'
import Stats from './components/common/stats/Stats'

export const AccountOverview = ({data}) => {
  console.log(data);
  return (
    <div className="AccountOverview">
        <Container>
          <TopContainer>
            <TitleContainer>
              <Title text="Account Overview" />
            </TitleContainer>
            <ContactDetailsContainer>
              <SupportDetails email={data.supportContact.email}  phone={data.supportContact.phone}/>
            </ContactDetailsContainer>
          </TopContainer>
          <BottomContainer>
            <Sales uploads={data.salesOverview.uploads} linesAdded={data.salesOverview.linesAttempted} />
            <SalesStatsContainer>
              <Stats data={data} type='successfulUploads' />
              <Stats data={data} type='linesSaved'  />
            </SalesStatsContainer>
          </BottomContainer>
        </Container>
    </div>
  )
}

export default AccountOverview;