import styled from 'styled-components';

export const Container = styled.div`
display: flex;
flex-direction: column;
flex-wrap: wrap;
flex-basis: 800px;
`
export const TitleContainer = styled.div`
display: flex;
flex: 1;
flex-wrap: wrap;
`
export const ContactDetailsContainer = styled.div`
align-content: space-between;
flex-wrap: wrap;
flex: 1;
`
export const TopContainer = styled.div`
display: flex;
align-items: center;
flex-wrap:wrap;
justify-content: space-around;
`
export const BottomContainer = styled.div`
display: flex;
flex-direction: column;
justify-content: space-between;
`
export const SalesStatsContainer = styled.div`
display: inline-flex;
column-gap: 2px;
margin-top: 2px;
`