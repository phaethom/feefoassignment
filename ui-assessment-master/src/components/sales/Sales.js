
import {Container,
    ContainerTop,
    IconContainer,
    IconTitleContainer,
    InfoIconContainer,
    BoldStyle,
    LeftContainer,
    RightContainer,
    Title} from './styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUpload, faInfoCircle } from '@fortawesome/free-solid-svg-icons';

const Sales = ({uploads, linesAdded})=>{
    return(
        <Container>
            <LeftContainer>
                <IconContainer>
                    <FontAwesomeIcon icon= {faUpload} size="1x" style={{ color: '#0099ff' }} ></FontAwesomeIcon>
                    <IconTitleContainer>
                        <Title >Sales</Title>
                    </IconTitleContainer>
                </IconContainer>
                <ContainerTop>
                    <span>You had <BoldStyle>{uploads} uploads</BoldStyle>  and <BoldStyle>{linesAdded}</BoldStyle> lines added.</span>
                </ContainerTop>
            </LeftContainer>
            <RightContainer>
               <InfoIconContainer>
                    <FontAwesomeIcon icon={faInfoCircle} size="sm" style={{ color: '#808080' }} ></FontAwesomeIcon>
               </InfoIconContainer>
            </RightContainer>
        </Container>
    )
}

export  {Sales};