import styled from 'styled-components';


export const Container = styled.div`
display: flex;
flex-direction: column;
background-color: white;
flex-wrap: wrap
align-content: space-around;
border-style: none;
border-radius: 5px 5px 0px 0px;
text-align: center;
margin-bottom: -19px;
height:10vh
`
export const ContainerTop = styled.div`
display: flex;
margin-left: 20px;
margin-top: 15px;
`
export const IconContainer = styled.div`
display: flex;
align-itens: flex-end;
margin-top: 20px;
margin-left: 20px;
`
export const IconTitleContainer =  styled.div`
display: flex;
margin-left: 10px;
`
export const InfoIconContainer = styled.div`
display: flex;
margin-right: 10px;
align-content: start;
`
export const BoldStyle = styled.span`
font-weight: bold;
`
export const LeftContainer = styled.div`
display: flex;
flex-direction: column;
flex-wrap: wrap;
flex: 1;
`
export const RightContainer = styled.div`
display: flex;
justify-content: flex-end;
flex: 1;
margin-top: -70px;
`
export const Title = styled.span`
font-weight: bold;
font-size: 22px;
`


