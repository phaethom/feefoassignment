import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPercentage } from '@fortawesome/free-solid-svg-icons';
import {Container, Value, Title} from './style'

const SaleDetails = ({title, value})=>{
    return(
        <Container>
            <Value>{value} <FontAwesomeIcon icon= {faPercentage}></FontAwesomeIcon></Value>
            <Title>{title}</Title>
        </Container>
    )
}

export  {SaleDetails};