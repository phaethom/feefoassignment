import {TitleStyle, Container} from './style'


const Title = ({text}) => {
return(
    <Container>
        <TitleStyle>
            {text}
        </TitleStyle>

    </Container>
)
}

export   {Title};