import { render, screen } from '@testing-library/react';
import {Title} from './Title';

test('set text', () => {
  const {container} = render(<Title text="Account Overview" />);
  expect(container.textContent).toBe('Account Overview');
});

test('render component without crashing', () => {
    const {container} = render(<Title text="Account Overview" />);
    expect(container.innerHTML).toMatchSnapshot();
  });
  