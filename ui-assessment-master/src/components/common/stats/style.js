import styled from 'styled-components';

export const Container = styled.div`
background-color: white;
border-style: none;
border-radius: ${(props)=> props.type==='successfulUploads' ? "0px 0px 0px 5px" :' 0px 0px 5px 0px'};
//border-radius: 0px 0px ${(props)=> props.type==='successfulUploads' ? " 0px 5px": '5px 0px' };
display: flex;
flex: 1;
flex-direction: column;
height: 10vh;
margin-top: 19px
`
export const Value = styled.span`
color: green;
font-size: 35px;
font-weight: bold;
`
export const Title = styled.span`
color: #666666;
font-weight: bold;
`
export const TitleContainer = styled.div`
display: flex;
margin-left: 20px;
`
export const ValueContainer = styled.div`
display: flex;
margin-left: 10px;
margin-left: 20px;
margin-top: 15px
`