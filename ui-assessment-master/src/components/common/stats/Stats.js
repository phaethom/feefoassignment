
import {Container, Value, Title, ValueContainer, TitleContainer} from './style'

const Stats = (props)=>{
    const {salesOverview}= props.data;
    console.log(props)
    return(
        <Container type={props.type}>
            <ValueContainer>
                <Value>{salesOverview[props.type]}<span>%</span></Value>
            </ValueContainer>
            <TitleContainer>
                <Title>{props.type=='successfulUploads' ? 'UPLOAD SUCCESS' : 'LINES SAVED'}</Title>
            </TitleContainer>
        </Container>
    )
}

export default Stats;

