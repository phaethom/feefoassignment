import {Container,
    Titlee,
    TitleContainer,
    EmailContainer,
    Email, IconContainer,
    ContactContainer,
    PhoneContainer,
    Phone,
    Support,
    SupportTitle } from './style';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import logo from '../img/logo.jpeg'

const SupportDetails = ({email,phone})=>{
    return(
        <Container>
            <TitleContainer>
                <Titlee>YOUR FEEFO SUPPORT CONTACT</Titlee>
            </TitleContainer>
            <ContactContainer>
                <IconContainer>
                    <img src={logo} style={{width:45}}/>
                </IconContainer>
                <EmailContainer>
                    <Support>
                        <SupportTitle>Support</SupportTitle>
                    </Support>
                    <Email> <FontAwesomeIcon  icon= {faEnvelope}/> {email}   </Email>
                </EmailContainer>
                <PhoneContainer>
                    <Phone>{phone}</Phone>
                </PhoneContainer>
            </ContactContainer> 
        </Container>
    )
}

export  {SupportDetails};