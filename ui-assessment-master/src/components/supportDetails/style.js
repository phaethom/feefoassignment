import styled from 'styled-components';


export const Container = styled.div`
`
export const TitleContainer =styled.div`
display: flex;
justify-content: flex-start;
`
export const PhoneContainer =styled.div`
display: flex;
align-self: center;
margin-right: 15px;
margin-top: 20px;
`
export const Phone = styled.span`
color: #666666;
`
export const Titlee =styled.h3`
font-size: 20px;
margin-top: 20px;
color: #666666;
`
export const EmailContainer =styled.div`
`
export const ContactContainer =styled.div`
display: flex;
justify-content: space-between;  
`
export const Email =styled.span`  
color: #666666;  
`
export const IconContainer = styled.div`

`
export const Support = styled.div`
display: flex;
`
export const SupportTitle = styled.span`
font-size: 20px;
font-weight: bold;
`
